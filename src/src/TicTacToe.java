import java.io.IOException;
import java.util.Scanner;
public class TicTacToe {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
        boolean x=true;
        int k=0;
        printBoard(board);
        while (x==true) {

            boolean y=true;
            boolean z=true;
            boolean o=true;
            boolean e;

            while(y==true){
                e=true;
                System.out.print("Player 1 enter row number:");
                int row = reader.nextInt();
                System.out.print("Player 1 enter column number:");
                int col = reader.nextInt();


                if((row>3 && col>3) || (row>3 || col>3)){
                    System.out.println("Please enter a row and col smaller than 4");
                    y=true;
                    e=false;
                }
                else if((board[row-1][col-1]=='X' || board[row-1][col-1]=='O')&& e==true) {
                    System.out.println("Please enter another coordinate.");
                    y = true;
                    e = false;

                }



                if( e==true && y==true) {
                    board[row - 1][col - 1] = 'X';

                    printBoard(board);
                    checkBoard(board);
                    y = false;
                    k=k+1;
                    if(k>=9 && checkBoard(board)==false) {
                        System.out.println("It's a draw!");
                        x =false;
                        y=false;
                        z=false;
                    }
                    if(checkBoard(board)==true){
                        System.out.println("Player 1 Won!");
                        x=false;
                        y=false;
                        z=false;
                    }


                }



            }
            while(z==true) {
                e=true;

                System.out.print("Player 2 enter row number:");
                int row2 = reader.nextInt();
                System.out.print("Player 2 enter column number:");
                int col2 = reader.nextInt();
                if((row2>3 && col2>3) || (row2>3 || col2>3)){
                    System.out.println("Please enter a row and col smaller than 4");
                    z=true;
                    e=false;
                }
                else if((board[row2-1][col2-1]=='X' || board[row2-1][col2-1]=='O')&& e==true){
                    System.out.println("Please enter another coordinate.");
                    z=true;
                    e=false;
                }
                if(((row2<=3 && 2<=3) || (row2<=3 || col2<=3))&& e==true) {
                    board[row2 - 1][col2 - 1] = 'O';
                    printBoard(board);
                    if(checkBoard(board)==true){
                        System.out.println("Player 2 Won!");
                        x=false;
                        y=false;
                        z=false;
                    }
                    z = false;
                    k=k+1;


                }



            }
            y=true;
            z=true;






        }
        reader.close();



    }

    public static void printBoard(char[][] board) {
        System.out.println("    1   2   3");
        System.out.println("   -----------");
        for (int row = 0; row < 3; ++row) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < 3; ++col) {
                System.out.print("|");
                System.out.print(" " + board[row][col] + " ");
                if (col == 2)
                    System.out.print("|");

            }
            System.out.println();
            System.out.println("   -----------");

        }


    }
    public static boolean checkBoard (char[][] board){

        if(board[0][0]=='X' && board[1][1]=='X' && board[2][2]=='X') {

            return true;


        }
        if(board[0][0]=='X' && board[1][0]=='X' && board[2][0]=='X'){
            return true;
        }
        if(board[0][0]=='X' && board[0][1]=='X' && board[0][2]=='X'){
            return true;

        }
        if (board[1][0]=='X' && board[1][1]=='X' && board[1][2]=='X') {
            return true;
        }
        if(board[2][0]=='X' && board[2][1]=='X' && board[2][2]=='X'){
            return true;
        }
        if(board[0][1]=='X' && board[1][1]=='X' && board[2][1]=='X') {
            return true;
        }
        if(board[0][2]=='X' && board[1][2]=='X' && board[2][2]=='X') {
            return true;
        }
        if(board[0][2]=='X' && board[1][1]=='X' && board[2][0]=='X') {
            return true;
        }
        if(board[0][0]=='X' && board[1][1]=='X' && board[2][2]=='X') {

            return true;


        }
        if(board[0][0]=='O' && board[1][0]=='O' && board[2][0]=='O'){
            return true;
        }
        if(board[0][0]=='O' && board[0][1]=='O' && board[0][2]=='O'){
            return true;

        }
        if (board[1][0]=='O' && board[1][1]=='O' && board[1][2]=='O') {
            return true;
        }
        if(board[2][0]=='O' && board[2][1]=='O' && board[2][2]=='O'){
            return true;
        }
        if(board[0][1]=='O' && board[1][1]=='O' && board[2][1]=='O') {
            return true;
        }
        if(board[0][2]=='O' && board[1][2]=='O' && board[2][2]=='O') {
            return true;
        }
        if(board[0][2]=='O' && board[1][1]=='O' && board[2][0]=='O') {
            return true;
        }
        if(board[0][0]=='O' && board[1][1]=='O' && board[2][2]=='O'){
            return true;
        }
        else{
            return false;
        }



    }

}
